const express = require('express');
const http = require('http')
// const session = require('express-session');
var cors = require('cors');
require('dotenv').config();

// Must load AFTER dotenv.
const routes = require('./routes/index');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());


const cachePolicy = { maxAge: (1000 * 60 * 60 * 24 * 365).toString()};
const shortCachePolicy = { maxAge: (1000 * 60 * 60 * 24).toString()};

if (process.env.NODE_ENV === 'production'){
    app.use('/static', express.static(__dirname + '/../client/static', cachePolicy));
    app.use(express.static(__dirname + '/../client', shortCachePolicy));
}
else
    app.use("/public", express.static(__dirname + '/../client/public/', shortCachePolicy));

app.use(routes);

require("./handleErrors")(app);

const PORT = 7777;
const server = http.createServer(app);

server.listen(PORT);
server.on('error', error => {
    console.error(error);

    if (error.syscall !== 'listen')
        throw error;

    switch (error.code) {
        case 'EACCES':
            console.error('Port ' + PORT + ' requires elevated privileges');
            process.exit(1);
        case 'EADDRINUSE':
            console.error('Port ' + PORT + ' is already in use');
            process.exit(1);
        default:
            throw error;
    }
});
server.on('listening', () => {
    const addr = server.address();
    console.log("PID %s listening on port %s.", process.pid, addr.port || addr);
});