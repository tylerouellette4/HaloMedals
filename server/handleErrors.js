const init = (app) => {

    /*
      Not Found Error Handler
     
      If we hit a route that is not found, we mark it as 404 and pass it along to the next error handler to display
    */
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });


    /*
      Development Error Handler
     
      In development we show good error messages so if we hit a syntax error or any other previously un-handled error, we can show good info on what happened
    */
    if (process.env.NODE_ENV !== 'production') {
        app.use((err, req, res, next) => {
            err.stack = err.stack || '';
            const errorDetails = {
                message: err.message,
                status: err.status,
                stackHighlighted: err.stack.replace(/[a-z_-\d]+.js:\d+:\d+/gi, '<mark>$&</mark>')
            };
            res.status(err.status || 500);
            res.format({
                // Based on the `Accept` http header
                'text/html': () => {
                    res.send(errorDetails);
                }, // Form Submit, Reload the page
                'application/json': () => res.json(errorDetails) // Ajax call, send JSON back
            });
        });
    }


    /*
      Production Error Handler
     
      No stacktraces are leaked to user
    */
    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: {}
        });
    });
}

module.exports = init;