const express = require('express');
const router = express.Router();
const axios = require('axios');
const fs = require('fs');
const path = require('path');

/*
  Catch Errors Handler

  With async/await, you need some way to catch errors
  Instead of using try{} catch(e) {} in each controller, we wrap the function in
  catchErrors(), catch any errors they throw, and pass it along to our express middleware with next()
*/
const route = (fn) => {
  return function (req, res, next) {
    return fn(req, res, next).catch(next);
  };
};


const token = process.env.STDLIB_SECRET_TOKEN;
const lib = require('lib')({ token: token }); // process.env.STDLIB_SECRET_TOKEN


router.get('/', route(async (req, res) => {
  let result = await lib.halo.infinite['@0.3.3'].stats['service-record'].multiplayer({
    gamertag: 'King Crucifyy',
    filter: 'matchmade:pvp'
  });

  const sorted = result.data.core.breakdowns.medals.sort((a, b) => b.count - a.count)
  result.data.core.breakdowns.medals = sorted;
  res.send(result);
}));

const rankProxyRoute = route(async (req, res, next) => {

  const fileName = req.params.fileName;
  const filePath = __dirname + '/../../client/public/static/images/ranks/' + fileName;
  const filePathProd = __dirname + '/../../client/static/images/ranks/' + fileName;

  // if (fs.access(filePath, constants.F_OK, (err) => {console.log(`${file} ${err ? 'does not exist' : 'exists'}`)}) || fs.access(filePathProd, constants.F_OK, (err) => {console.log(`${file} ${err ? 'does not exist' : 'exists'}`)}) )
  try {
    let response = await axios({
      url: "https://assets.halo.autocode.gg/static/infinite/images/multiplayer/playlist-csrs/" + fileName,
      method: 'GET',
      responseType: 'stream',
      responseEncoding: 'binary'
    });
    
    response.data.pipe(fs.createWriteStream(filePath));
    // res.header('Cache-Control', 'public, max-age=6000');
    res.sendFile((process.env.NODE_ENV === 'development' ? filePath : filePathProd));
  }
  catch (ex) {
    console.log(ex);
    if (ex.response && ex.response.status != 404)
      console.log(ex);
    next(ex);
  }
})

const medalsProxyRoute = route(async (req, res, next) => {

  const fileName = req.params.fileName;
  const filePath = __dirname + '/../../client/public/static/images/medals/medium/' + fileName;
  const filePathProd = __dirname + '/../../client/static/images/medals/medium/' + fileName;

  try {
    let response = await axios({
      url: "https://assets.halo.autocode.gg/static/infinite/images/multiplayer/medals/medium/" + fileName,
      method: 'GET',
      responseType: 'stream',
      responseEncoding: 'binary'
    });

    response.data.pipe(fs.createWriteStream(filePath));
    // res.setHead('Cache-Control', 'public, max-age=6000000');
    res.sendFile((process.env.NODE_ENV === 'development' ? filePath : filePathProd));
  }
  catch (error) {
    console.log(error);
    if (error.response && error.response.status != 404)
      console.log(error);
    next(error);
  }

})

router.get('/public/static/images/medals/medium/:fileName', medalsProxyRoute);
router.get('/static/images/medals/medium/:fileName', medalsProxyRoute);
router.get('/public/images/ranks/:fileName', rankProxyRoute);
router.get('/static/images/ranks/:fileName', rankProxyRoute);

router.get('/api/medals/:id', route(async (req, res) => {
  const gamertag = decodeURI(req.params.id);
  
  
  let result = await lib.halo.infinite['@0.3.9'].stats['service-record'].multiplayer({
    gamertag,
    filter: 'matchmade:pvp'
  });
  
  const sorted = result.data.core.breakdowns.medals.sort((a, b) => b.count - a.count);
  result.data.core.breakdowns.medals = sorted;
  res.send(result);
}));

router.get('/api/csr/:id', route(async (req, res) => {
  const gamertag = decodeURI(req.params.id);
  let result = await lib.halo.infinite['@0.3.9'].stats.csrs({
    gamertag,
    season: 1,
    version: 2
  });

  const sorted = result.data.sort((a, b) => b.response.current.value - a.response.current.value);
  result.data = sorted;
  res.send(result);
}));

router.get('/*', route(async (req, res) => {
  // res.sendFile(path.join(__dirname, '../public/index.html'));
  res.sendFile(path.join(__dirname, '../../../build', '/client/index.html'));
  // res.sendFile((process.env.NODE_ENV === 'development' ? __dirname + '../../client/public/index.html' : __dirname + '../../client/public/index.html'));
}));

module.exports = router;