FROM node:current

COPY --from=joh43990.sprint.dynatracelabs.com/linux/oneagent-codemodules:nodejs / /
ENV LD_PRELOAD /opt/dynatrace/oneagent/agent/lib64/liboneagentproc.so

WORKDIR /usr/local/halomedals

COPY . .

ENV NODE_ENV=production

RUN apt-get update

# Install git if missing.
RUN apt-get install git -y -qq

RUN npm i
RUN npm i -g pm2
RUN npm run build

EXPOSE 7777/tcp
EXPOSE 22/tcp

# Start the pm2 daemon with the application config.
CMD ["pm2-runtime", "ecosystem.config.js"]
