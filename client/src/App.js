import React from 'react';
import { Switch, Route, Routes, BrowserRouter } from 'react-router-dom';
import Matches from './components/Matches';
import Compare from './components/Compare';
import Medals from './components/Medals.js'
import NotFound from './components/NotFound';


export default function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<Medals />} />
                <Route path='medals' element={<Medals />} />
                <Route path='medals/:id' element={<Medals />} />
                <Route path='matches' element={<Matches />} />
                <Route path='matches/:id' element={<Matches />} />
                <Route path='compare' element={<Compare />} />
                <Route path='*' element={<NotFound />} />
            </Routes>
        </BrowserRouter>
    );
}