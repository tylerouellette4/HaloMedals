import React from 'react'
import styled from 'styled-components';


const TimerStyles = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 20px;
    margin: 10px;
    max-height: 100px;
    min-width: 145px;
    border-radius: 0.5rem;
    background-color: var(--main-bg);
    color: var(--theme-color);
    font-size: 2rem;
    font-family: 'Rajdhani Medium';

    p {
        margin: 0;
    }
    div > div {
        font-size: 2rem;
        font-weight: 700;
    }

    @media (max-width: 1686px) {
        flex-grow: 1;
	}
    @media (max-width: 835px) {
		font-size: 2rem;
        padding: 20px;
        max-height: 100px;
        flex-grow: 1;
	}
`

export default function Timer({timeInSeconds, refreshTime}) {

    return (
        <TimerStyles>
            <div>Refreshes In: <div>{ Math.floor(timeInSeconds/60) }:{ (timeInSeconds%60 < 10 || timeInSeconds === refreshTime) ? `0${ timeInSeconds%60 }` : timeInSeconds%60 } </div> </div>
        </TimerStyles>
    )
}
