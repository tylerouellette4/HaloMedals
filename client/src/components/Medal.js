import React from 'react'
import allMedalsJSON from '../allMedalsJSON.json'
import MedalStyles from '../components/styles/MedalStyles';


function Medal({props, rarity, type}) {

    const description = allMedalsJSON.filter( medal => medal.id === props.id ? medal.description : null);

    const medalType = allMedalsJSON.find( medal => medal.id === props.id ? medal : null);
    const medalRarity = allMedalsJSON.find( medal => medal.id === props.id ? medal.rarity : null);
    const isDevMode = process.env.NODE_ENV === "development";
    // const imgUrl = props.image_urls.small.slice(80);
    const imgUrl = props.image_urls.medium.slice(81);
    // console.log(props.image_urls.medium.slice(81))

    // ALL MEDALS
    if (rarity === 'All' && type === 'All'){
        return (
            <MedalStyles className={medalRarity.rarity}>
                <div className={'backdrop'}></div>
                <h3>{!props ? "Loading..." : props.name}</h3>
                <img src={(isDevMode ? "/public/static/images/medals/medium/" : "/static/images/medals/medium/") + imgUrl} alt={props.name} loading='lazy'/>
                <h2 className={'count'}>{!props ? 0 : props.count}</h2>
                <p className="description">{!description ? "Loading..." : description[0].description}</p>
            </MedalStyles>
        )
    }

    // Filtered by Type
    if (rarity === 'All' && type !== 'All'){
        if (medalType.type === type) {
            return (
                <MedalStyles className={medalRarity.rarity}>
                    <div className={'backdrop'}></div>
                    <h3>{!props ? "Loading..." : props.name}</h3>
                    <img src={(isDevMode ? "/public/static/images/medals/medium/" : "/static/images/medals/medium/") + imgUrl} alt={props.name} loading='lazy' />
                    <h2 className={'shrink'}>{!props ? 0 : props.count}</h2>
                    <p className="description">{!description ? "Loading..." : description[0].description}</p>
                </MedalStyles>
            )
        }
        else {
            return (null)
        }
    }	

    // Filtered Rarity
    if (type === 'All' && rarity !== 'All'){
        if (medalRarity.rarity === rarity) {
            return (
                <MedalStyles className={medalRarity.rarity}>
                    <div className={'backdrop'}></div>
                    <h3>{!props ? "Loading..." : props.name}</h3>
                    <img src={(isDevMode ? "/public/static/images/medals/medium/" : "/static/images/medals/medium/") + imgUrl} alt={props.name} loading='lazy' />
                    <h2 className={'shrink'}>{!props ? 0 : props.count}</h2>
                    <p className="description">{!description ? "Loading..." : description[0].description}</p>
                </MedalStyles>
            )
        }
        else {
            return (null)
        }
    }
    // Filtered by BOTH
    if (type !== 'All' && rarity !== 'All'){
        if (medalType.type === type && medalRarity.rarity === rarity) {
            return (
                <MedalStyles className={medalRarity.rarity}>
                    <div className={'backdrop'}></div>
                    <h3>{!props ? "Loading..." : props.name}</h3>
                    <img src={(isDevMode ? "/public/static/images/medals/medium/" : "/static/images/medals/medium/") + imgUrl} alt={props.name} loading='lazy' />
                    <h2 className={'shrink'}>{!props ? 0 : props.count}</h2>
                    <p className="description">{!description ? "Loading..." : description[0].description}</p>
                </MedalStyles>
            )
        }
        else {
            return (null)
        }
    }
}

export default Medal
