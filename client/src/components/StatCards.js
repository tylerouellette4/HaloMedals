import React from 'react'
import styled from 'styled-components';

const StatCard = styled.div`
    display: flex;
    justify-content: center;
    border-radius: 0.5rem;
    padding: 10px;
    box-shadow: 0 3px 6px rgb(0 0 0 / 15%);
    font-size: 1.25rem;
    border-radius: 0.5rem;
    background: var(--main-bg);
    color: var(--theme-color);
    font-family: 'Rajdhani Medium';

    div > h3 {
        margin: 0;
        font-size: 1.25rem;
    }

    div {
        font-size: 1.5rem;
    }
`

function StatCards({allStats}) {
    var summary = [];
    if (allStats){
        summary = allStats.core.summary;
        delete summary.vehicles
        if (summary.kills){
            summary.kd = (summary.kills / summary.deaths) > 1 ? (summary.kills / summary.deaths).toPrecision(3) : (summary.kills / summary.deaths).toPrecision(2);
        }
        else {
            summary.kd = 0;
        }
        summary.timePlayed = allStats.time_played.human;
    } 
    return (
        <>
            <StatCard> <div> Kills: <h3>{summary.kills} </h3></div></StatCard>
            <StatCard> <div> Deaths: <h3>{summary.deaths} </h3></div></StatCard>
            <StatCard> <div> KD Ratio: <h3>{summary.kd} </h3></div></StatCard>
            <StatCard> <div> Assists: <h3>{summary.assists} </h3></div></StatCard>
            <StatCard> <div> Suicides: <h3>{summary.suicides} </h3></div></StatCard>
            <StatCard> <div> Betrayals: <h3>{summary.betrayals} </h3></div></StatCard>
        </>
    )
}

export default StatCards
