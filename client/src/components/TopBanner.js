import React from 'react';
import Logo from './Logo';
import TopBannerStyles from '../components/styles/TopBannerStyles'

export function TopBanner() {

    const isDevMode = process.env.NODE_ENV === "development";

    return (
        <TopBannerStyles>
            <Logo><img alt='Logo' src={isDevMode ? 'http://localhost:7777/public/static/images/HMLogo.svg' : '../static/images/HMLogo.svg'}></img></Logo>
        </TopBannerStyles>
    )
}
export default TopBanner;