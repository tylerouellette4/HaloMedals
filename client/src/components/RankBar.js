import Rank from "./Rank";
import RankBarStyles from "./styles/RankBarStyles";


const RankBar = ({csr, rankFilter}) => {
	return (
		<RankBarStyles>
				{!csr ? '' : csr.map((csr, index) => <Rank csr={csr} rankFilter={rankFilter} key={index} />)}
		</RankBarStyles>
	)
}

export default RankBar;