import React from 'react';
import Medal from './Medal';
import AllMedalsStyles from './styles/AllMedalsStyles';



function OwnedMedalGallery({allStats,medal,rarity,type,index}) {
  return <AllMedalsStyles>
      {!allStats ? null : allStats.core.breakdowns.medals.map((medal, index) => <Medal props={medal} rarity={rarity} type={type} key={index} />)}
  </AllMedalsStyles>;
}


export default OwnedMedalGallery;
