import styled from "styled-components";

const MissingMedalsStyles = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(225px, 1fr));
	grid-gap: 20px;
	padding: 10px;
	margin: 0 10px;
	opacity: 0.7;

	@media (max-width: 560px) {
        grid-template-columns: repeat(auto-fit, minmax(175px, 1fr));
	}
`;

export default MissingMedalsStyles