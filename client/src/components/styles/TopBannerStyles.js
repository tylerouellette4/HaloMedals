import styled from 'styled-components';

const TopBannerStyles = styled.nav`
    font-size: 1.5rem;
    color: var(--theme-color);
    background: var(--main-bg);
    padding: 10px;
    font-family: 'Plateia-Bold';
    
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    align-content: center;

    path {
        stroke: var(--theme-color);
    }

    h1 {
        margin: 20px;
    }

    a:active,a:hover,a:selected {
        color: wheat;
    }

    @media (max-width: 930px){
       display: flex;
       flex-direction: column;
       justify-content: center;
       align-items: center;

       img {
           min-width: 400px;
       }
       h1 {
           margin: 10px;
       }
    }
    @media (max-width: 530px){
       display: flex;
       flex-direction: column;
       justify-content: center;
       align-items: center;

       img {
           min-width: 300px;
       }
       
       h1 {
           margin: 10px;
       }
    }
`;

export default TopBannerStyles;