import styled from 'styled-components'

const MedalStyles = styled.div`
    border: 3px solid black;
    box-shadow: 0 3px 6px rgb(0 0 0 / 15%);
    font-size: 1.25rem;
    border-radius: 1rem;
    color: #dadddb;
    font-family: 'Rajdhani Medium';

    transition: all 400ms ease-in-out;
    position: relative;
    z-index: 1;
    overflow: hidden;
    background-blend-mode: exclusion;

    display: flex;
    flex-direction: column;
    align-items: center;
    
    >* {
        z-index: 2;
        position: relative;
    }

    p {
        margin: 0.2rem;
        font-size: 0.7rem;
    }

    h2 {
        background: var(--secondary-bg);
        border-radius: 1rem;
    }
    h2,h3 {
        margin-top: 10px;
        margin-bottom: 0px;
        padding: 5px;
    }
    h3 {
        font-size: 2rem;
        min-height: 95px;
    }

    img {
        border-radius: 1rem;
        padding: 10px;
        /* background-color: var(--secondary-bg); */
        width: 125px;
    }

    .backdrop {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        z-index: 2;
        background: var(--main-bg);
    }

    .count {
        display: block;
        margin: auto;
        width: 125px;
        text-align: center;
        color: var(--theme-color);
    }

    .shrink {
        width: 125px;
    }

    :before,
    :after {
        content: "";
        position: absolute;
        height: 10px;
        width: 25%;
        background-color: #b19d7c0f;
        z-index: 5;
        transition: all 400ms ease-in-out;
    }

    :before {
        top: 0;
        right: -5px;
        border-left: 2px solid var(--theme-color);
        transform: skewX(20deg);
    }

    :after {
        bottom: 0;
        left: -5px;
        border-right: 2px solid var(--theme-color);
        transform: skewX(20deg);
    }

    :hover {
        border-color: var(--theme-color);
        box-shadow: #ccc 0 0 2px;
        background-position-y: 100%;
    }

    :hover:before,
    :hover:after {
        width: 110%;
    }

    .description {
        font-size: 1rem;
        opacity: 0;
        padding: 2px;
        min-height: 45px;
        transition: all 200ms ease-in-out;
    }
    :active {
        .description {
            display: flex;
            justify-content: center;
            color: white;
        }
    }
    :hover {
        .description {
            opacity: 1;
            display: flex;
            justify-content: center;
            color: white;
        }
    }

    :checked {
        .description {
            display: flex;
            justify-content: center;
            color: white;
            /* font-size: 0.7rem; */
        }
    }


    &.Mythic {
        border: 0;
        img {
            background: radial-gradient(#371215, #37121588, #AB7F2D50, #AB7F2D40, #AB7F2D20, #AB7F2D00 55px);
        }
    }

    &.Mythic .backdrop {
        border: 0;
        z-index: 1;
    }

    &.Mythic .backdrop::before {
        content: "";
        position: absolute;
        animation: Mythic 15.5s linear infinite;
        border-radius: 1rem;
        background: repeating-linear-gradient(75deg, #AB7F2D, #AB9F2F, #371215 200px);
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: -1;
        background-size: 200% 200%;
    }

    &.Mythic .backdrop::before {
        bottom: 0;
    }

    &.Mythic .backdrop::after {
        content: "";
        position: absolute;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        height: auto;
        width: auto;
        z-index: -1;
        border-radius: 1rem;
        margin: 3px;
        background: inherit;
    }

    @keyframes Mythic  {
        0% {
            background-position: 0% 50%;
        }

        50% {
            background-position: 100% 50%;
        }

        100% {
            background-position: 200% 50%;
        }
    }


    &.Legendary {
        border: 0;
        img {
            background: radial-gradient(#953eae,#953eae88, #953eae66, #953eae44, #29145f66, #953eae00 55px);
        }
    }

    &.Legendary .backdrop {
        border: 0;
        z-index: 1;
    }

    &.Legendary .backdrop::before {
        content: "";
        position: absolute;
        border-radius: 1rem;
        background: repeating-linear-gradient(75deg, #29145f, #953eae 500px);
        opacity: 0.5;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: -1;
        background-size: 200% 200%;
    }

    &.Legendary .backdrop::before {
        bottom: 0;
    }

    &.Legendary .backdrop::after {
        content: "";
        position: absolute;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        height: auto;
        width: auto;
        z-index: -1;
        border-radius: 1rem;
        margin: 3px;
        background: inherit;
    }

    @keyframes Legendary  {
        0% {
            background-position: 0% 50%;
        }

        50% {
            background-position: 100% 50%;
        }

        100% {
            background-position: 200% 50%;
        }
    }

    //Heroic
    &.Heroic {
        border: 0;

        img {
            background: radial-gradient(#1dbbe5, #1dbbe566, #1dbbe544, #14318144, #14318100 55px);
        }
    }

    &.Heroic .backdrop {
        border: 0;
        z-index: 1;
    }

    &.Heroic .backdrop::before {
        content: "";
        position: absolute;
        border-radius: 1rem;
        background: repeating-linear-gradient(75deg, #141620, #143181, #1dbbe5 500px);
        opacity: 0.5;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: -1;
        background-size: 200% 200%;
    }

    &.Heroic .backdrop::before {
        bottom: 0;
    }

    &.Heroic .backdrop::after {
        content: "";
        position: absolute;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        height: auto;
        width: auto;
        z-index: -1;
        border-radius: 1rem;
        margin: 3px;
        background: inherit;
    }

    @keyframes Heroic  {
        0% {
            background-position: 0% 50%;
        }

        50% {
            background-position: 100% 50%;
        }

        100% {
            background-position: 200% 50%;
        }
    }
    
    //Normal
    &.Normal {
        border: 0;

        img {
            background: radial-gradient(#3adb3a55, #3adb3a40, #3adb3a22, #953eae00 50px);
        }
    }

    &.Normal .backdrop {
        border: 0;
        z-index: 1;

        
    }

    &.Normal .backdrop::before {
        content: "";
        position: absolute;
        border-radius: 1rem;
        background: repeating-linear-gradient(75deg, #3adb3a, #2E2017, #2A872A 400px);
        opacity: 0.5;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: -1;
        background-size: 200% 200%;
    }

    &.Normal .backdrop::before {
        bottom: 0;
    }

    &.Normal .backdrop::after {
        content: "";
        position: absolute;
        bottom: 0;
        top: 0;
        left: 0;
        right: 0;
        height: auto;
        width: auto;
        z-index: -1;
        border-radius: 1rem;
        margin: 3px;
        background: inherit;
    }

    @keyframes Normal  {
        0% {
            background-position: 0% 50%;
        }

        50% {
            background-position: 100% 50%;
        }

        100% {
            background-position: 200% 50%;
        }
    }



    @media (max-width: 983px){
        .description {
            opacity: 1;
            font-size: 0.9rem;
        }
        h3 {
            min-height: 92px;
        }
    }

    @media (max-width: 560px) {
        h3 {
            min-height: 65px;
            font-size: 1.5rem;
        }
    }
`

export default MedalStyles