import styled from "styled-components";

const FilterBarStyles = styled.div`
	display: flex;
	justify-content: flex-start;
	flex-wrap: wrap;
	gap: 20px;
	padding: 10px;
	margin: 10px;
	font-family: 'Rajdhani Medium';

	label {
		min-height: 25px;
		padding: 10px;
		color: var(--theme-color);
		font-weight: 600;
		font-size: 2rem;
	}

	select {
		min-height: 50px;
		min-width: 100px;
		padding: 10px;
		border-radius: 0.5rem;
		font-size: 1.5rem;
		background-color: var(--main-bg);
		color: var(--theme-color);
		border-style: none;
	}

	option {
		border-style: none;
	}

	.container {
		display: flex;
		gap: 20px;
		flex: 1;
	}

	form {
		display: grid;
		place-content: center;
	}

	.form-control + .form-control {
		margin-top: 1em;
	}

	input[type="checkbox"] {
		/* Add if not using autoprefixer */
		-webkit-appearance: none;
		/* Remove most all native input styles */
		appearance: none;

		color: var(--theme-color);
		width: 50px;
		height: 50px;
		border: 0.15em solid currentColor;
		border-radius: 0.15em;
		transform: translateY(-0.075em);

		display: grid;
		justify-self: center;
		place-content: center;
	}

	input[type="checkbox"]::before {
	content: "";
		width: 0.75em;
		height: 0.75em;
		clip-path: polygon(14% 44%, 0 65%, 50% 100%, 100% 16%, 80% 0%, 43% 62%);
		transform: scale(0);
		transform-origin: bottom left;
		transition: 120ms transform ease-in-out;
		background-color: var(--theme-color);
		font-size: 2rem;
	}

	input[type="checkbox"]:checked::before {
		transform: scale(1);
	}

	input[type="checkbox"]:focus {
		outline: max(1px) solid var(--main-bg);
		outline-offset: max(1px);
	}

	@media (max-width: 1550px) {
		label {
			font-size: 1.5rem;
		}
	}

	@media (max-width: 1355px) {
		label {
			font-size: 1.2rem;
		}
	}
	@media (max-width: 1260px) {

		display: grid;
		grid-template-columns: 1fr 1fr;

	}
	@media (max-width: 700px) {
		display: grid;
		grid-template-columns: 1fr 1fr;
		justify-content: center;
		font-size: 2rem;

		label {
			min-height: 75px;
			font-size: 1.5rem;
		}
	}
	@media (max-width: 560px) {
		display: grid;
		grid-template-columns: 1fr;
		justify-items: center;
		label {
			font-size: 1.5rem;
		}
	}
	@media (max-width: 460px) {
		
		display: flex;
		label {
			min-height: 50px;
			font-size: 1.25rem;
		}
	}
	@media (max-width: 380px) {

		select {
			width: 125px;
		}
	
		label {
			font-size: 1rem;
		}
	}
`

export default FilterBarStyles;