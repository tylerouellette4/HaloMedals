
import { createGlobalStyle } from "styled-components";

const FontStyles = createGlobalStyle`

	@font-face {
		font-family: 'Rajdhani Medium';
		src: url('/static/fonts/Rajdhani-Medium.ttf') format('truetype');
		font-weight: 500;
		font-style: normal;
	}
	@font-face {
		font-family: 'Plateia-Bold';
		src: url('/static/fonts/Plateia-Bold.ttf') format('truetype');
		font-weight: 500;
		font-style: normal;
	}
	
	letter-spacing: 2px;
	font-family: 'Rajdhani Medium';
	font-weight: 500;
`;

export default FontStyles;