import styled from "styled-components";

const MatchesBarStyles = styled.div`
	display: grid;
	grid-template-columns: repeat(auto-fit, minmax(125px, 1fr));
	grid-gap: 20px;
	padding: 10px;
	margin: 0 10px;
	grid-auto-flow: row dense;

	div > h3 {
			font-size: 1.5rem;
		}
	div {
		font-size: 1.5rem;
	}

	@media (max-width: 1505px) {
		grid-template-columns: 1fr 1fr 1fr;
		
		div > h3 {
			font-size: 1.5rem;
		}
		div {
			font-size: 1rem;
		}
	}

	@media (max-width: 435px) {
		grid-template-columns: 1fr 1fr;

		div > h3 {
			font-size: 0.8rem;
		}
		div {
			font-size: 1.2rem;
		}
	}
`;

export default MatchesBarStyles