import styled from "styled-components";

const RankFilterBarStyles = styled.div`
	display: flex;
	flex-wrap: wrap;
	justify-content: flex-start;
	gap: 20px;
	padding: 10px;
	margin: 10px;
	font-family: 'Rajdhani Medium';

	label {
		min-height: 25px;
		padding: 10px;
		color: var(--theme-color);
		font-weight: 600;
		font-size: 2rem;
	}

	select {
		min-height: 50px;
		min-width: 250px;
		padding: 10px;
		border-radius: 0.5rem;
		font-size: 1.5rem;
		background-color: var(--main-bg);
		color: var(--theme-color);
		border-style: none;
	}

	option {
		border-style: none;
	}

	form {
		display: grid;
		place-content: center;
	}

	.form-control + .form-control {
		margin-top: 1em;
	}

	input[type="checkbox"] {
        /* Add if not using autoprefixer */
        -webkit-appearance: none;
        /* Remove most all native input styles */
        appearance: none;

        color: var(--theme-color);
        width: 50px;
        height: 50px;
        border: 0.15em solid currentColor;
        border-radius: 0.15em;
        transform: translateY(-0.075em);

        display: grid;
        justify-self: center;
        place-content: center;
	}

	input[type="checkbox"]::before {
	content: "";
		width: 0.75em;
		height: 0.75em;
		clip-path: polygon(14% 44%, 0 65%, 50% 100%, 100% 16%, 80% 0%, 43% 62%);
		transform: scale(0);
		transform-origin: bottom left;
		transition: 120ms transform ease-in-out;
		background-color: var(--theme-color);
		font-size: 2rem;
	}

	input[type="checkbox"]:checked::before {
		transform: scale(1);
	}

	input[type="checkbox"]:focus {
		outline: max(1px) solid var(--main-bg);
		outline-offset: max(1px);
	}

	@media (max-width: 660px) {
		font-size: 2rem;
        display: flex;
        justify-content: center;
        gap: 0;
        
        form {
            display: flex;
            flex-direction: column;
            
            width: 85%;
        }

		label {
			font-size: 1.5rem;
		}

        select {
            flex: grow;
        }
	}
	@media (max-width: 300px) {
	
		label {
			min-height: 110px;
			font-size: 1.5rem;
		}
	}
`

export default RankFilterBarStyles;