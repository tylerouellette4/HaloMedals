import styled from 'styled-components';


const MedalStatsCardStyles = styled.div` 
  display: flex;
  justify-content: flex-end;
  flex: 1;
  align-items: center;
  font-size: 1.5rem;
  gap: 20px;
  grid-column: 1 / 5;
  grid-row: 3;
  margin: 20px;
  flex-wrap: wrap;

  div {
      background: var(--main-bg);
      padding: 10px;
      border-radius: 0.5rem;
      color: var(--theme-color);
      font-size: 1.5rem;
      min-width: 400px;
      flex: 1;
  }

  h3 {
      margin: 0;
  }

  @media (max-width: 1640px) {
      div {
          flex: 1;
          min-height: 85px;
          min-width: 350px;
        }
        justify-content: flex-start;
    }
    @media (max-width: 960px) {
        div {
            flex: 1;
            min-height: 85px;
            min-width: 250px;
            font-size: 1.25rem;

      }
      justify-content: center;
  }
  @media (max-width: 560px) {
      div {
          min-width: 250px;
          font-size: 1.25rem;
      }
      justify-content: center;
  }
  @media (max-width: 400px) {
      div {
          min-width: 100px;
          font-size: 1rem;
      }
  }
  @media (max-width: 330px) {
    div {
        min-width: 100px;
        font-size: 1rem;
    }
    
    div > div {
        min-height: 110px;
    }
  }
`

export default MedalStatsCardStyles