import styled from 'styled-components';

const AppStyles = styled.div`
    * {
        box-sizing: border-box;
        --main-bg: #121212;
        --secondary-bg: #1c1c1c;
        --theme-color: #b19d7c;
        --small-text-color: #414141;
        --highlight-color: #ffffff;
	}

    html, body {
		background: repeat center/100% url('/static/images/GrungeBG.png');
	}
`;

export default AppStyles;