import styled from 'styled-components';

const BannerStyles = styled.div`

	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	flex: 0 0;
	justify-content: center;
	margin: 15px;
	padding: 15px;
	gap: 10px;
	font-size: 1.5rem;

	font-family: 'Plateia-Bold';
	
	img {
		width: 125px;
		height: 125px;
	}

	h2,h3 {
		color: var(--theme-color);
		margin: 0;
		text-transform: uppercase;
	}

	h2 {
		font-size: 4rem;
		line-height: 6rem;
		transform: scale3d(1.05,1,1);
	}

	h3 {
		margin-top: -1rem;
	}
	
	h1 {
		font-size: 1rem;
	}
	
	div {
		display: flex;
		flex-direction: column;
	}

	@media (max-width: 1550px) {

		img {
			width: 75px;
			height: 75px;
		}

		h2,h3 {
			color: var(--theme-color);
			margin: 0;
			text-transform: uppercase;
			font-size: 1rem;
		}

		h3 {
			font-size: 1rem;
		}

		h2 {
			font-size: 2rem;
			line-height: 3rem;
			transform: scale3d(1.05,1,1);
		}
	}
	@media (min-width: 1000px) and (max-width: 1549px) {

		img {
			width: 75px;
			height: 75px;
		}

		h2,h3 {
			color: var(--theme-color);
			margin: 0;
			text-transform: uppercase;
		}

		h3 {
			font-size: 1rem;
		}

		h2 {
			font-size: 2rem;
			line-height: 3rem;
			transform: scale3d(1.05,1,1);
		}
	}
	@media (max-width: 625px) {

		img {
			width: 50px;
			height: 50px;
		}

		h2,h3 {
			color: var(--theme-color);
			margin: 0;
			text-transform: uppercase;
			font-size: 0.8rem;
		}

		h3 {
			font-size: 0.8rem;
		}

		h2 {
			font-size: 1.6rem;
			line-height: 2rem;
			transform: scale3d(1.05,1,1);
		}
	}
	@media (max-width: 500px) {

		img {
			display: none;
		}

		h2,h3 {
			color: var(--theme-color);
			margin: 0;
			text-transform: uppercase;
		}

		h3 {
			font-size: 1rem;
		}

		h2 {
			font-size: 1.5rem;
			line-height: 2rem;
			transform: scale3d(1.05,1,1);
		}
	}
`

export default BannerStyles;