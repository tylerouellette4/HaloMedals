import styled from "styled-components"

const FormStyles = styled.form`
    display: flex;
	flex-wrap: wrap;
    justify-content: center;
    align-items: center;
	color: var(--theme-color);
	background: no-repeat center/100% url('/static/images/GrungeBG.png');

	label {
		padding: 10px;
		background: var(--main-bg);
		border-radius: 0.5rem;
		font-size: 1.25rem;
		margin: 15px;
	}
	input[type=text] {
		height: 100px;
		min-width: 50%;
		border-radius: 0.5rem;
		padding: 25px;
		background: var(--main-bg);
		color: var(--theme-color);
		margin: 15px;
		/* min-width: fit-content; */
		font-size: 2rem;
		border-style: none;
		font-family: 'Rajdhani Medium';
	}
	input[type=submit] {
		border-radius: 0.5rem;
		height: 100px;
		padding: 25px;
		margin: 15px;
		background: var(--main-bg);
		color: var(--theme-color);
		font-size: 2rem;
		border-style: none; 

		display: block;
		/* margin: 0 auto; */
		/* width: 50%; */
		border-bottom: 5px solid #B19d7c;
		box-shadow: 0px 2px 10px #121212;
		transition: 150ms ease;
		text-align: center;
		font-weight: bold;
		
		:active {
			border: none;
			border-bottom: 2px solid steelblue;
			box-shadow: 0px 1px 5px grey;
			background: var(--theme-color);
			color: var(--main-bg);
			border-bottom: 5px solid var(--main-bg);
		}
	}

	@media (max-width: 640px) {
		display: grid;
		grid-template-columns: 0.95fr;

		input[type=text] {
			font-size: 1.5rem;
		}
	}
	@media (max-width: 440px) {
		input[type=text] {
			font-size: 1.2rem;
		}
	}
`

export default FormStyles