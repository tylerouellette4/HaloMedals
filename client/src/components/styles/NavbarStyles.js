import styled from 'styled-components';

const NavbarStyles = styled.nav`
    display: flex;
    justify-content: space-around;
    align-items: center;
    background-color: var(--main-bg);
    padding-bottom: 15px;
    margin: 10px;
    border-radius: 0.5rem;
    font-family: Plateia-Bold;
    a {
        color: var(--theme-color);
    }
` 
export default NavbarStyles;