import styled from "styled-components";

const InfoBarStyles = styled.div`
	margin-bottom: 10px;
	padding: 10px;
	display: none;

	@media (max-width: 700px) {
		display: flex;
		flex-shrink: 0;
		border-radius: 0.5rem;
		background-color: var(--main-bg);
		color: var(--theme-color);
		margin: 15px;
		
		.info {
			font-size: 1.25rem;
			padding: 10px;
			margin: 10px;
		}
	}
	
`;

export default InfoBarStyles