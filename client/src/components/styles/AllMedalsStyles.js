import styled from "styled-components";

const AllMedalsStyles = styled.div`
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(225px, 1fr));
	grid-gap: 20px;
	padding: 10px;
	margin: 0 10px;

	@media (max-width: 560px) {
        grid-template-columns: repeat(auto-fit, minmax(175px, 1fr));
	}
`;

export default AllMedalsStyles;