import styled from "styled-components";

const SelfPromoBarStyles = styled.div`
	padding: 10px;
	font-size: 2rem;
	background-color: #0A0A0A;
	color: var(--theme-color);
	font-family: 'Plateia-Bold';
	text-align: center;
	
	a {
		color: var(--theme-color);
	}

	@media (min-width: 1000px) and (max-width: 1500px){
		font-size: 1.5rem;
	}
	@media (max-width: 1000px){
		font-size: 1rem;
	}
`

export default SelfPromoBarStyles