import styled from 'styled-components';


const FullPageStyles = styled.div`

    height: 100%;
    background: repeat center/100% url('/static/images/GrungeBG.png');

    @media (min-width: 1550px){
        display: grid;
        grid-template-columns: 300px 1fr 300px;
    }
    @media (min-width: 900px) and (max-width: 1550px){
        display: grid;
        grid-template-columns: 1px 1fr 1px;
    }
    
` 


export default FullPageStyles;
