import styled from "styled-components"

const RankStyles = styled.div`
	display: flex;
    flex-wrap: wrap;
    justify-content: center;
    padding: 15px;
    margin: 10px 0;
    background-color: var(--main-bg);
    color: var(--theme-color);
    border-radius: 1rem;
    z-index: 2;
    text-transform: uppercase;

    p {
        margin: 5px;
    }
    
    .Rank {
        width: 100px;
        height: 100px;
        margin: 10px;
    }
    
    .tier {
        min-width: 156px;
        font-weight: 750;
        font-family: 'Plateia-Bold';
        font-size: 1.2rem;
    }
    
    .value {
        border-radius: 0.5rem;
        letter-spacing: .15em;
        font-weight: 350;
        font-family: 'Rajdhani Medium';
    }

    .input {
        font-family: 'Rajdhani Medium';
    }
    
    .queue {
        letter-spacing: .15em;
        font-weight: 600;
        font-family: 'Rajdhani Medium';
    }
    
    .QueueCSR {
        margin: 10px;
        border-radius: 6px;
        background-color: var(--secondary-bg);
        min-height: 110px;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-self: center;
        flex: 1 1 100px;
        min-width: 150px;
    }
    
    .ranknTier {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: start;
        min-width: 156px;
        flex: 1 1 125px;
        text-align: left;
    }

    @media (min-width: 1516px) and (max-width: 1675px) {
        
       .Rank {
           width: 50px;
           height: 50px;
       }
        
	}
    @media (max-width: 430px) {
        
        display: flex;
        max-width: 375px;
        min-width: 375px;

        .tier,.value {
            letter-spacing: .10em;
            text-transform: uppercase;
            font-weight: 350;
            font-family: 'Rajdhani Medium';
        }
        
	}
`

export default RankStyles