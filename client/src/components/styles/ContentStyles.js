import styled from "styled-components";

const ContentStyles = styled.div`
	text-align: center;
	background: repeat center/100% url('/static/images/GrungeBG.png');
	min-height: 80vh;
	display: flex;
	flex-direction: column;
	align-items: space-around;
	font-size: calc(10px + 2vmin);
	color: white;
	overflow-x: hidden;
	font-family: 'Rajdhani Medium';

`;

export default ContentStyles;