import styled from "styled-components";

const RankBarStyles = styled.div`
	display: grid;
	grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
	justify-content: center;
	align-content: center;
	padding: 10px;
	grid-gap: 2%;
	margin: 10px;

	p {
		font-size: 1.45rem;
	}

	@media (min-width: 851px) and (max-width: 1515px){
		grid-template-columns: 1fr;
		gap: 0;
	}

	@media (min-width: 431px) and (max-width: 850px){
		grid-template-columns: 1fr;
		gap: 0;
	}

	@media (max-width: 430px) {
		grid-template-columns: repeat(auto-fit, minmax(375px, 1fr));
		gap: 0%;
	}
`;

export default RankBarStyles;