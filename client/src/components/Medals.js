import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Medal from './Medal';
import TopBanner from './TopBanner';
import axios from 'axios';
import Timer from './Timer';
import FlashMessage from 'react-flash-message';
import { Flash } from './Flash';
import FontStyles from './styles/FontStyles';
import allMedalsJSON from '../allMedalsJSON.json'
import RankBar from './RankBar';
import FilterBarStyles from './styles/FilterBarStyles'
import MissingMedalStyles from './styles/MissingMedalStyles'
import FormStyles from './styles/FormStyles'
import InfoBar from './InfoBar';
import Banner from './Banner';
import SelfPromoBar from './SelfPromoBar';
import Tooltip from './Tooltip';
import StatsBar from './StatsBar';
import MatchesBar from './MatchesBar';
import Footer from './Footer'
import MissingMedalHeader from './MissingMedalHeader';
import RankFilterBarStyles from './styles/RankFilterBarStyles';
import OwnedMedalGallery from './OwnedMedalGallery';
import MedalStatsBar from './MedalStatsBar';
import ContentStyles from './styles/ContentStyles';
import FullPageStyles from './styles/FullPageStyles';
import AppStyles from './styles/AppStyles';
import Gamertag from './Gamertag';
import Navbar from './Navbar';

export default function Medals() {
	const navigate = useNavigate();
	const params = useParams();

	const [allStats, setAllStats] = React.useState(null);
	const [medals, setMedals] = React.useState(null);
	const [csr, setCSR] = React.useState('');
	const [GT, setGT] = React.useState(params && params.id ? decodeURI(params.id) : '');
	const [staticGT, setStaticGT] = React.useState(params && params.id ? decodeURI(params.id) : '');
	const [myInterval, setMyInterval] = React.useState(1000);
	const [csrInterval, setCsrInterval] = React.useState(1000);
	const [timeInSeconds, setTimeInSeconds] = React.useState(300);
	const [counter, setCounter] = React.useState(timeInSeconds);
	const [flash, setFlash] = React.useState(false);
	const [errors, setErrors] = React.useState(null);
	const [rarity, setRarity] = React.useState('All');
	const [type, setType] = React.useState('All');
	const [rankFilter, setRankFilter] = React.useState('current');
	const [showOwned, setShowOwned] = React.useState(true);
	const [showNotOwned, setShowNotOwned] = React.useState(false);

	const rarityMap = {
		"Normal": 1,
		"Heroic": 2,
		"Legendary": 3,
		"Mythic": 4
	}

	const handleRarityFilter = (e) => {
		e.preventDefault();
		setRarity(e.target.value);
		const filteredRarity = allMedalsJSON.filter((medal) => {
			if (e.target.value === 'All') {
				return medal;
			}
			if (type !== 'All') {
				if (medal.rarity === e.target.value && medal.type === type) {
					return medal;
				}
			}
			else if (medal.rarity === e.target.value) {
				return medal;
			}
		})

		const sortedRarity = filteredRarity.sort((a, b) => b.count - a.count)
		setMedals(sortedRarity)
	}

	const handleTypeFilter = (e) => {
		e.preventDefault();
		setType(e.target.value);
		const filteredType = allMedalsJSON.filter((medal) => {
			if (e.target.value === 'All') {
				return medal;
			}
			if (rarity !== 'All') {
				if (medal.type === e.target.value && medal.rarity === rarity) {
					return medal;
				}
			}
			else if (medal.type === e.target.value) {
				return medal;
			}
		})
		const filterSorted = filteredType.sort((a, b) => b.count - a.count);
		setMedals(filterSorted);
	}

	const handleRankFilter = (e) => {
		e.preventDefault();
		setRankFilter(e.target.value);
	}

	const handleSubmit = (e) => {
		e.preventDefault();
		// const escaped = /[!@#$%^&*()`_+\-=\[\]{};':"\\|,.<>\/?]+/;

		const escaped = new RegExp([
			'[^',
			// "\u0021-\u0026", // Basic Latin symbols	"!"-"&"
			// "\u0028-\u002F", // Basic Latin symbols	"("-"/"
			// "\u003A-\u0040", // Basic Latin symbols	":"-"@"
			// "\u005B-\u0060", // Basic Latin symbols	"["-"`"
			// "\u007B-\u007E", // Basic Latin symbols	"{"-"~"
			// "\u00A0-\u00BF", // Latin supplemental symbols	NBSP-"¿"
			"\u0020",    	 // Space	
			"\u0027",    	 // Apostrophe	'
			"\u0030-\u0039", //	Arabic numbers	"0"-"9"
			"\u0041-\u005A", //	Capital letters	"A"-"Z"
			"\u0061-\u007A", //	Lowercase letters	"a"-"z"
			"\u00C0-\u00F6", //	Latin Supplemental	"À"-"ö"
			"\u00F8-\u00FF", //	Latin Supplemental	"ø"-"ÿ"
			"\u0100-\u017F", //	Latin Extended-A	"Ā"-"ſ"
			"\u1100-\u1112", //	Hangul Jamo (consonants)
			"\u1161-\u1175", //	Hangul Jamo (vowels)
			"\u11A8-\u11C2", //	Hangul Jamo (consonants with consonant clusters)
			"\uAC00-\uD7A3", //	Hangul syllables
			"\u3041-\u3096", //	Hiragana
			"\u30A1-\u30FA", //	Katakana
			"\u4E00-\u9FFF", //	CJK Unified Ideographs
			"\u0400-\u045F", //	Core Cyrillic alphabet
			"\u0985-\u09B9", //	Bengali
			"\u0E01-\u0E3A", //	Thai
			"\u0E40-\u0E4E", //	Thai
			"\u0E01-\u0E30", //	Thai characters
			"\u0E32-\u0E33", //	Thai characters
			"\u0E40-\u0E46", //	Thai characters
			"\u0E31",        //	Thai diacritic
			"\u0E34-\u0E3A", //	Thai diacritics
			"\u0E47-\u0E4E", //	Thai diacritics
			"\u0390-\u03CE", //	Greek
			"\u0900-\u094F", //	Devanagari
			"\u0966-\u096F", //	Devanagari numbers
			"\u0671-\u06D3", //	Urdu (Arabic)
			"\u06F0-\u06F9", //	Urdu numbers
			"\u0904-\u0939", //	Devanagari characters
			"\u0900-\u0903", //	Devanagari diacritics
			"\u093A-\u094F", //	Devanagari diacritics
			"\u0620-\u064A", //	Core Arabic alphabet
			"\u0660-\u0669", //	Arabic numbers
			"\u05D0-\u05EA", //	Hebrew alphabet
			"]"
		].join(''));

		if (GT && GT.trim().length > 0 && !escaped.test(GT)) {
			setStaticGT(GT.trim());
			setCounter(300);
			setFlash(false);

		} else if (GT && escaped.test(GT)) {
			setErrors('Nice try but not today, get those special characters out of there');
			setFlash(true);
		}
		else {
			setErrors('Enter a Gamertag');
			setFlash(true);
		}
	}

	const handleFlashChange = (state) => {
		setFlash(state);
	}

	const handleShowOwned = () => setShowOwned(!showOwned);
	const handleShowNotOwned = () => setShowNotOwned(!showNotOwned);

	const getAllStats = () => {
		if (staticGT.length > 0) {
			axios.get(`/api/medals/${encodeURI(staticGT)}`).then((res) => res.data).then((allStats) => {
				setAllStats(allStats.data);
				navigate(`/medals/${encodeURI(staticGT)}`);
			}).catch((error) => {
				setErrors('Enter a Valid Gamertag, if your gamertag has spaces, use spaces. It should match what it looks like in game.');
				setFlash(true);
				clearInterval(myInterval)
			});
		}
		clearInterval(myInterval)
		setCounter(300);
	}

	const getCSR = () => {
		if (staticGT.length > 0) {
			axios.get(`/api/csr/${encodeURI(staticGT)}`).then((res) => res.data).then((csr) => {
				setCSR(csr.data)
				// history.pushState(`/csr/${encodeURI(staticGT)}`);
			}).catch((error) => {
				clearInterval(csrInterval)
			});
			clearInterval(csrInterval)
		}
	}
	// 1s x 60 seconds 8 5 = 5 min - Used for setInterval on how often to refresh the data
	const refreshTime = 1000 * 60 * 5;

	React.useEffect(() => {
		if (staticGT) {
			getAllStats();
			getCSR();
		}
		setMyInterval(setInterval(getAllStats, refreshTime));
		setCsrInterval(setInterval(getCSR, refreshTime));
	}, [staticGT]);

	React.useEffect(() => {
		const timer = setInterval(() => counter === 0 ? setCounter(timeInSeconds) : setCounter(counter - 1), 1000);
		return () => clearInterval(timer);
	}, [counter, timeInSeconds]);

	return (
		<AppStyles>
			<FontStyles />
			<TopBanner />
			<SelfPromoBar />
			<FullPageStyles>
				<div></div>
				<ContentStyles>
					<Banner />
					<FormStyles onSubmit={handleSubmit}>
						<input type="text" value={GT} name="GT" placeholder={ params && params.id ? params.id : 'Enter your in game name'} onChange={e => setGT(e.target.value)}></input>
						<input type="submit" value="Submit" onSubmit={handleSubmit}></input>
						<Tooltip />
						{flash === true ? <FlashMessage duration={5500}><Flash flash={flash} message={errors} flashFunc={handleFlashChange} /></FlashMessage> : ''}
						<Timer timeInSeconds={counter} refreshTime={timeInSeconds}></Timer>
					</FormStyles>
					<InfoBar />
					{/* <Navbar /> */}
					<RankFilterBarStyles>
						<form>
							<label htmlFor='rankType'>Filter your rank</label>
							<select value={rankFilter} onChange={handleRankFilter} disabled={!csr}>
								<option value="current">Current Rank</option>
								<option value="season">Best This Season</option>
								<option value="all_time">Best All Time</option>
							</select>
						</form>
						{staticGT ? <Gamertag staticGT={staticGT} /> : null}
					</RankFilterBarStyles>
					{csr && csr !== '' ? <RankBar csr={csr} rankFilter={rankFilter} /> : null}
					{!allStats ? null : <StatsBar allStats={allStats} />}
					{!allStats ? null : <MatchesBar allStats={allStats} />}
					<FilterBarStyles>
						<div className='container'>

							<form>
								<label htmlFor='rarity'>Select a Rarity </label>
								<select value={rarity} onChange={handleRarityFilter} disabled={!allStats}>
									<option>All</option>
									<option>Normal</option>
									<option>Heroic</option>
									<option>Legendary</option>
									<option>Mythic</option>
								</select>
							</form>
							<form>
								<label htmlFor='type'>Select a Type </label>
								<select value={type} onChange={handleTypeFilter} disabled={!allStats} >
									<option>All</option>
									<option>Assist</option>
									<option>General</option>
									<option>Equipment</option>
									<option>Match</option>
									<option>Melee</option>
									<option>Multi</option>
									<option>Nades</option>
									<option>Objective</option>
									<option>Spree</option>
									<option>Vehicle</option>
									<option>Weapon</option>
								</select>
							</form>
						</div>

						<div className='container'>
							<form>
								<label htmlFor='showOwned'>Show Owned</label>
								<input type='checkbox' name='showOwned' value={showOwned} onChange={handleShowOwned} checked={showOwned}></input>
							</form>
							<form>
								<label htmlFor='showNotOwned'>Show Not Owned</label>
								<input type='checkbox' name='showNotOwned' value={showNotOwned} onChange={handleShowNotOwned} checked={showNotOwned}></input>
							</form>
						</div>
					</FilterBarStyles>
					{!allStats ? null : (<MedalStatsBar allStats={allStats} />)}
					{!showOwned ? null : (
						<OwnedMedalGallery allStats={allStats} rarity={rarity} type={type} />
					)}
					{showNotOwned ? (
						<>
							<MissingMedalHeader />
							<MissingMedalStyles>
								{!medals ? allMedalsJSON.sort((a, b) => rarityMap[a.rarity] - rarityMap[b.rarity]).map((medal, index) => {
									if (allStats) {

										const ownedMedals = allStats.core.breakdowns.medals;
										if (ownedMedals.some(ownedMedal => ownedMedal.id === medal.id)) {
											return (null);
										}
										else {
											return <Medal props={medal} rarity={rarity} type={type} key={index} />
										}
									} else return null;
								}) : medals.sort((a, b) => rarityMap[a.rarity] - rarityMap[b.rarity]).map((medal, index) => {
									const ownedMedals = allStats.core.breakdowns.medals;
									if (ownedMedals.some(ownedMedal => ownedMedal.id === medal.id)) {
										return (null);
									}
									else {
										return <Medal props={medal} rarity={rarity} type={type} key={index} />
									}
								})}
							</MissingMedalStyles>
						</>
					) : null}
				</ContentStyles>
				<div></div>
			</FullPageStyles>
			<Footer />
		</AppStyles>
	);
}
