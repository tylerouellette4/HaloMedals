import React from 'react'
import Footer from './Footer'
import SelfPromoBar from './SelfPromoBar'
import AppStyles from './styles/AppStyles'
import FontStyles from './styles/FontStyles'
import TopBanner from './TopBanner'

function Layout({children}) {
  return (
    <>
        <AppStyles>
			<FontStyles />
			<TopBanner />
			<SelfPromoBar />
            {children}
            <Footer />
        </AppStyles>
    </>
  )
}

export default Layout