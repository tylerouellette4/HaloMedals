import React from 'react';
import styled from 'styled-components';

const FooterStyles = styled.footer`
    display: flex;
    justify-content: flex-end;
    align-items: center;
    color: black;
    background: var(--main-bg);
    font-weight: 800;
    color: var(--theme-color);
    height: 125px;
    width: 100%;
    z-index: 1;
    font-size: calc(1.5rem + 2vmin);
    text-align: center;
    font-family: 'Rajdhani Medium';
    
    .cpy {
        position: absolute;
        width: 100%;
    }
    
    @media (max-width: 1000px) {
        display: flex;
        flex-direction: column;
        font-size: calc(1.0rem + 1vmin);
        height: 150px;
        
        .cpy {
            position: relative;
        }
    }
`

const Socials = styled.div`
    display: flex;
    align-items: center;
    text-align: center;
    margin: 20px;
    gap: 10px;
    
    img {
        height: 48px;
        width: 48px;
        background-color: var(--theme-color);
        border-radius: 1rem;
        position: relative;
    }
    a {
        z-index: 2;
        position: relative;
    }
`


function Footer() {

    const isDevMode = process.env.NODE_ENV === "development";

    return <FooterStyles>
        <div className='cpy'>Copyright &copy; HaloMedals.com 2022</div>
        <Socials>
                <div><a href={'https://twitter.com/HaloMedals'}><img alt='Twitter Logo' src={ isDevMode ? 'http://localhost:7777/public/static/images/twitterLogo.svg' : "/static/images/twitterLogo.svg"} /> </a> </div>
                <div><a href={'https://instagram.com/tylerouellette'} ><img alt='Instagram Logo' src={isDevMode ? 'http://localhost:7777/public/static/images/instagramLogo.svg' : "/static/images/instagramLogo.svg"} /></a></div>
                {/* <div><a href={'https://github.com/tyler-ouellette'}><img src={'../../public/static/images/githubLogo.svg'} /> </a></div> */}
                <div><a href={'https://twitch.tv/kingcrucify'} ><img alt='Twitch Logo' src={isDevMode ? 'http://localhost:7777/public/static/images/twitchLogo.svg' : "/static/images/twitchLogo.svg"}></img> </a></div>
        </Socials>
    </FooterStyles>;
}



export default Footer;
