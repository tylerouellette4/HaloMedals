import React from 'react';
import SelfPromoBarStyles from './styles/SelfPromoBarStyles';

function SelfPromoBar() {
	return (
		<>
			<SelfPromoBarStyles>Follow me on Twitch - <a href={'https://twitch.tv/kingcrucify'}>twitch.tv/kingcrucify</a></SelfPromoBarStyles>
			<SelfPromoBarStyles>Bookmark the page to quickly view your medals!</SelfPromoBarStyles>
		</>
	)
}

export default SelfPromoBar;
