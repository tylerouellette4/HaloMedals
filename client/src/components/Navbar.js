import React from 'react'
import { Link } from 'react-router-dom';
import NavbarStyles from './styles/NavbarStyles';


function Navbar() {
    return (
        <NavbarStyles>
            <Link to="medals">Medals</Link>
            <Link to="matches">Matches</Link>
            <Link to="compare">Compare</Link>
        </NavbarStyles>
    )
}

export default Navbar