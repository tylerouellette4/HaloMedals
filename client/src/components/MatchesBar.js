import React from 'react';
import MatchesCards from './MatchesCards';
import MatchesBarStyles from './styles/MatchesBarStyles';

function MatchesBar({allStats}) {
  return <MatchesBarStyles>
      { !allStats ? null : <MatchesCards allStats={allStats} /> }
  </MatchesBarStyles>
}

export default MatchesBar;
