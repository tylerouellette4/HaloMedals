
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

const FlashStyles = styled.div`
    
    .alert {
        color: white;
        position: absolute;
        top: 6.5%;
        right: 10px;
        padding: 15px;
        display: flex;
        align-items: center;
        z-index: 1111;

        @media (max-width: 1550px) {
            top: -25px
        }
        @media (max-width: 1100px) {
            top: 0px
        }
        @media (max-width: 500px) {
            top: 400px
        }
    }
    .alert p {
        margin: 0;  
        border: 1px solid black;
    }
    .alert-error {
        background: lightcoral;
    }
    .close {
        color: white;
        cursor: pointer;
        margin-right: 10px;
    }
`

export const Flash = ({flash, message, flashFunc}) => {
    let [visibility, setVisibility] = useState(flash);
    let [thismessage, setMessage] = useState(message);
    let [type, setType] = useState('Error');

    useEffect(() => {
        setVisibility(true);
        setMessage(thismessage);
        setType(type);
        setTimeout(() => {
            setVisibility(false);
            flashFunc(false);
        }, 5500);
    }, [flash, message]);

    useEffect(() => {
        if (document.querySelector('.close') !== null) {
            document.querySelector('.close').addEventListener('click', () => {flashFunc(false); setVisibility(false); });
        }
    })

    return (
        visibility && 
        <FlashStyles >
            <p className='alert alert-error'>{message}
            <span className="close"> &nbsp;&times;</span>
            </p>
        </FlashStyles>
    )
}