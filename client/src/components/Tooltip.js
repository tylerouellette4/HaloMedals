import React from 'react';
import styled from 'styled-components';
import ReactTooltip from 'react-tooltip';

const ToolTipStyles = styled.div`
    background-color: var(--main-bg);
    color: var(--theme-color);
    margin: 15px;
    padding: 5px;
    font-size: 1.8rem;
    width: 40px;
    height: 40px;
    border-radius: 50%; 
    font-family: 'Rajdhani Medium';
    align-self: start;
    
    p {
        border-radius: 0.5rem;
        padding: 5px 0;
        background-color: var(--main-bg);
        text-align: center;
        z-index: 1;
    }
    
    @media (max-width: 700px) {
        display: none;
    }    
    `;

const StyledReactTooltip = styled(ReactTooltip)`
  background-color: var(--main-bg) !important;
  color: var(--theme-color) !important;
  box-shadow: 0px 2px 20px var(--main-bg) !important;
  &:after {
      border-top-color: white !important;
    }
    max-width: 400px !important;
    font-size: 1.5rem !important;
    margin-right: 10px !important ;
`;

function Tooltip() {
    return (
        <>
            <ToolTipStyles data-tip data-for="tip">
                ?
            </ToolTipStyles>
            <StyledReactTooltip id="tip" effect="solid" place="bottom">
                <span>Ensure you are using your in game halo name. New Gamertags get numbers added if they aren't unique. If you search your name and get a rank but no stats or medals, you have your match history set to private. To fix this, in-game go to Settings {'>'} Acceessibility {'>'} Scroll to the bottom {'>'} Share</span>
            </StyledReactTooltip>
        </>
    );
}

export default Tooltip;
