import React from 'react';
import styled from 'styled-components';

const GTStyles = styled.h1`
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
    color: var(--theme-color);
    min-height: 131px;
    font-size: 3rem;
    text-align: center;
    font-family: 'Plateia-Bold';
    margin: 0;

    @media (max-width: 700px){
        min-height: 80px;
    }

    @media (max-width: 550px){
        min-height: 80px;
        font-size: 2rem;
    }
`
function Gamertag({staticGT}) {
    return <GTStyles>{staticGT}</GTStyles>;
}

export default Gamertag;
