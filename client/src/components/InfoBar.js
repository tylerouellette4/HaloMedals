import React from 'react';
import InfoBarStyles from './styles/InfoBarStyles';

function InfoBar() {
    return (
        <InfoBarStyles>
            <p className="info">Ensure you are using your in game halo name as it displays in game. If you search your name and get a rank but no stats or medals, you have your match history set to private. <br /> To fix this, in-game go to Settings {'>'} Accessibility {'>'} Scroll to the bottom {'>'} Share</p>
        </InfoBarStyles>
    )
}

export default InfoBar;
