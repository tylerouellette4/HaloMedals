import React from 'react';
import styled from 'styled-components';

const HeaderStyles = styled.h1`
    color: var(--theme-color);
    font-size: 5rem;
    font-family: 'Plateia-Bold';
    margin: 10px;
    padding: 10px;

    @media (max-width: 1175px){
        font-size: 3rem;
    }
    @media (max-width: 750px){
        font-size: 2rem;
    }
`

function MissingMedalHeader() {
  return <HeaderStyles>Not Owned Medals</HeaderStyles>;
}

export default MissingMedalHeader;
