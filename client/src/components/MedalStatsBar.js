import React from 'react';
import MedalStatsCardStyles from './styles/MedalStatsCardStyles';
import allMedalsJSON from '../allMedalsJSON.json'

function MedalStatsBar({allStats}) {
    return (
        <MedalStatsCardStyles>
            <div>Total Number of Medals: <h3>{allStats.core.summary.medals} </h3> </div>
            <div> Medal Collection Earned: <h3>{allStats.core.breakdowns.medals.length} / {allMedalsJSON.length}</h3></div>
        </MedalStatsCardStyles>
    )
}

export default MedalStatsBar;
