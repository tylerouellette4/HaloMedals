import React from 'react';
import StatsBarStyles from './styles/StatsBarStyles';
import StatCards from './StatCards';

function StatsBar({ allStats }) {
    return (
        <StatsBarStyles>
            {!allStats ? null : <StatCards allStats={allStats} />}
        </StatsBarStyles>
    ) 
}

export default StatsBar;
