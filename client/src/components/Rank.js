import React from 'react'
import RankStyles from './styles/RankStyles.js';


function Rank({csr, rankFilter}) {
    const rank = csr;
    const isDevMode = process.env.NODE_ENV === "development";

    if (rankFilter === "current") {
        const imgUrl = rank.response.current.tier_image_url.slice(81);
        return (
            <RankStyles>
                <img className='Rank' src={(isDevMode ? "http://localhost:7777/public/static/images/ranks/" : "/static/images/ranks/") + imgUrl} alt='Rank Logo' loading='lazy'/>
                <div className="ranknTier">
                    <p className='tier'>{rank.response.current.tier + ` ${(rank.response.current.tier !=="Unranked" && rank.response.current.tier !=="Onyx" ? rank.response.current.sub_tier : ``)}`}</p>
                    <p className='input'>{rank.input === 'mnk' ? 'KBM' : rank.input}</p>
                </div>
                <div className='QueueCSR'>
                    <p className='queue'>{rank.queue}</p>
                    <p className='value'>{rank.response.current.value === -1 ? `Matches to Rank: ` + rank.response.current.measurement_matches_remaining : rank.response.current.value + ' CSR'}</p>
                </div>
            </RankStyles>
        )
    }
    if (rankFilter === "all_time"){
        const imgUrl = rank.response.all_time.tier_image_url.slice(81);
        return (
            <RankStyles>
                <img className='Rank' src={(isDevMode ? "http://localhost:7777/public/static/images/ranks/" : "/static/images/ranks/") + imgUrl} alt='Rank Logo' loading='lazy'/>
                <div className="ranknTier">
                    <p className='tier'>{rank.response.all_time.tier + ` ${(rank.response.all_time.tier !=="Unranked" && rank.response.all_time.tier !=="Onyx" ? rank.response.all_time.sub_tier : ``)}`}</p>
                    <p className='input'>{rank.input === 'mnk' ? 'KBM' : rank.input}</p>
                </div>
                <div className='QueueCSR'>
                    <p className='queue'>{rank.queue}</p>
                    <p className='value'>{rank.response.all_time.value === -1 ? `Matches to Rank: ` + rank.response.all_time.measurement_matches_remaining : rank.response.all_time.value + ' CSR'}</p>
                </div>
            </RankStyles>
        )
    }
    if (rankFilter === "season"){
        const imgUrl = rank.response.season.tier_image_url.slice(81);
        return (
            <RankStyles>
                <img className='Rank' src={(isDevMode ? "http://localhost:7777/public/static/images/ranks/" : "/static/images/ranks/") + imgUrl} alt='Rank Logo' loading='lazy'/>
                <div className="ranknTier">
                    <p className='tier'>{rank.response.season.tier + ` ${(rank.response.season.tier !=="Unranked" && rank.response.season.tier !=="Onyx" ? rank.response.season.sub_tier : ``)}`}</p>
                    <p className='input'>{rank.input === 'mnk' ? 'KBM' : rank.input}</p>
                </div>
                <div className='QueueCSR'>
                    <p className='queue'>{rank.queue}</p>
                    <p className='value'>{rank.response.season.value === -1 ? `Matches to Rank: ` + rank.response.season.measurement_matches_remaining : rank.response.season.value + ' CSR'}</p>
                </div>
            </RankStyles>
        )
    }
}

export default Rank
