import styled from 'styled-components';


const Logo = styled.div`
    display: grid;
    justify-content: start;
    margin: 10px;
    max-height: fit-content;
    min-height: fit-content;
    
    @media (min-width: 930px){

        img {
            max-width: 300px;
            max-height: fit-content;
            justify-self: center;
            align-self: center;
        }
    }
    @media (max-width: 930px){
        img {
            max-width: 200px;
            max-height: fit-content;
            justify-self: center;
            align-self: center;
        }
    }
    /* grid-area: Logo; */
    /* background: no-repeat center/100% url('../../public/static/images/HMLogo.png'); */
`

export default Logo;
