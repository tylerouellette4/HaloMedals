import React from 'react'
import styled from 'styled-components';

const MatchCard = styled.div`
    display: flex;
    justify-content: center;
    border-radius: 0.5rem;
    padding: 10px;
    box-shadow: 0 3px 6px rgb(0 0 0 / 15%);
    font-size: 1.25rem;
    border-radius: 0.5rem;
    background: var(--main-bg);
    color: var(--theme-color);
    font-family: 'Rajdhani Medium';

    div > h3 {
        margin: 0;
        font-size: 1.25rem;
    }

    div {
        font-size: 1.5rem;
    }
`

function MatchesCards({allStats}) {
    var summary = [];
    if (allStats){
        summary = allStats.core.summary;
        delete summary.vehicles
        summary.kd = (summary.kills / summary.deaths).toPrecision(3);
        summary.timePlayed = allStats.time_played.human;
    } 

    return (
        <>
            {/* { !allStats ? <Loader /> : summaryArray.map(([key, value], index) => <StatCard key={index}> <h3> {Capitalize(key)}: </h3> <h3> &nbsp;{value} </h3> </StatCard>)} */}
            <MatchCard> <div> Time Played: <h3>{allStats.time_played.human} </h3></div></MatchCard>
            <MatchCard> <div> Games Played: <h3>{allStats.matches_played} </h3></div></MatchCard>
            <MatchCard> <div> Win Rate: <h3>{allStats.win_rate.toPrecision(2)}% </h3></div></MatchCard>
            <MatchCard> <div> Avg Dmg Dealt: <h3>{(allStats.core.damage.dealt / allStats.matches_played).toPrecision(4)}</h3></div></MatchCard>
            <MatchCard> <div> Avg Dmg Taken: <h3>{(allStats.core.damage.taken / allStats.matches_played).toPrecision(4)}</h3></div></MatchCard>
            <MatchCard> <div> Accuracy: <h3>{allStats.core.shots.accuracy.toPrecision(3)}%</h3></div></MatchCard>
        </>
    )
}

export default MatchesCards
