import React from 'react';
import BannerStyles from './styles/BannerStyles';

function Banner() {

 const isDevMode = process.env.NODE_ENV === "development";
  return (
    <BannerStyles>
    <img alt='Halo Medals Logo' className={'icon'} src={isDevMode ? 'http://localhost:7777/public/static/images/ReverseIcon.png' : '../static/images/ReverseIcon.png'}></img>
    <div>
        <h2>Medal Gallery</h2>
        <h3>Checkout Your Halo Infinite Medals</h3>
    </div>
    </BannerStyles>
  );
}

export default Banner;
